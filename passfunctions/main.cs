using System;
using static System.Console;
using static System.Math;

public class main{
	public static void Main(){
		for(int k=1; k<=3; k++){
			WriteLine($"Printing table");
			passfunctions.make_table(x => Sin(k*x), 0, 2*PI, 0.1);
		}
	}
}
