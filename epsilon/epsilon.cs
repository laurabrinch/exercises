using System;
using static System.Console;
using static System.Math;

public static class main{
	static void Main(){
		int i=1; while(i+1>i) {i++;}
		Write("my max int = {0}\n",i);
		Write($"int.MaxValue = {int.MaxValue}\n");

		int n=1; while(n-1<i) {n++;}
		Write("my min int = {0}\n",n);
		Write($"int.MinValue = {int.MinValue}\n");

		double x=1; while(1+x!=1){x/=2;} x*=2;
		Write("Machine epsilon for double = {0}\n",x);

		float y=1F; while((float)(1F+y) != 1F){y/=2F;} y*=2F;
		Write("Machine epsilon for float = {0}\n",y); //man kan også skrive Write($"Machine epsilon for float = {y}\n");

		Write($"System.Math.Pow(2,-52) = {System.Math.Pow(2,-52)}\n");
		Write($"System.Math.Pow(2,-23) = {System.Math.Pow(2,-23)}\n");
	
		int k=(int)1e6;
		double epsilon=Pow(2,-52);
		double tiny=epsilon/2;
		double sumA=0,sumB=0;

		sumA+=1; for(int j=0;j<k;j++){sumA+=tiny;}
		WriteLine($"sumA-1 = {sumA-1:e} should be {k*tiny:e}\n");

		for(int j=0;j<k;j++){sumB+=tiny;} sumB+=1;
		WriteLine($"sumB-1 = {sumB-1:e} should be {k*tiny:e}\n");

		WriteLine($"a=1, b=2 returns: {approx(1,2)}");
	}
	
	public static bool approx(double a, double b, double tau=1e-9, double epsilon=1e-9){
		if(Abs(a-b) < tau) {return true;} 
		if(Abs(a-b)/(Abs(a)+Abs(b))<epsilon) {return true;} 
		else {return false;}
	}
}
