using static System.Math;
using static System.Console;
using static cmath;

class main{
	static void Main(){
		complex a = -1;
		WriteLine($"sqrt(-1)={sqrt(a)}");
		WriteLine($"sqrt(i)={sqrt(I)}");
		WriteLine($"e^i={exp(I)}");
		WriteLine($"e^(i*pi)={exp(I*PI)}");
		WriteLine($"i^i={cmath.pow(I,I)}");
		WriteLine($"ln(i)={log(I)}");
		WriteLine($"sin(i*pi)={sin(I*PI)}");
	}


}
