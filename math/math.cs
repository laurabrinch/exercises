using System;
using static System.Console;
using static System.Math;

public static class math{
	public static int n=7;
	public static double pi=3.1415927;
	public static double e=2.71828;
	public static double sqrt2=Sqrt(2.0);
	public static double eipi=Exp(pi);
	public static double piie=Pow(pi,e);
}

public static class main{
	static string s="main\n"; 
	static void hello(){
		string s="hello\n";
		Write(s);
		}
	static int Main(){
		double x,y,sqrt2,eipi,piie;
		x=math.pi;
		y=math.e;
		sqrt2=math.sqrt2;
		eipi=math.eipi;
		piie=math.piie;
		Write("x={0} y={1}\n", x, y);//to måder at gøre den samme ting på
		Write($"x={x} y={y}\n");
		Write($"sqrt(2)={sqrt2}\n");
		Write($"eipi={eipi}\n");
		Write($"piie={piie}\n");
		Write($"sqrt2*sqrt2 = {sqrt2*sqrt2} (should be equal 2)\n");
		Write($"ln(eipi) = {Log(eipi)} (should be equal pi)\n");
		Write($"ln(piie)/ln(pi) = {Log(piie)/Log(x)} (should be equal e)\n");
	return 0;
	}
}
